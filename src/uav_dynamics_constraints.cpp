/**
 * Copyright (c) 2015 Carnegie Mellon University, Sanjiban Choudhury <sanjiban@cmu.edu>
 *
 * For License information please see the LICENSE file in the root directory.
 *
 */


#include "uav_dynamics_constraints/uav_dynamics_constraints.h"

namespace ca {

void UAVDynamicsConstraints::DynamicsConstraintsFromMsg(const uav_dynamics_constraints::DynamicsConstraints &msg) {
  if (msg.max_velocity.size() == 3)
    max_velocity = Eigen::Vector3d(msg.max_velocity[0], msg.max_velocity[1], msg.max_velocity[2]);
  if (msg.max_accel.size() == 3)
    max_accel = Eigen::Vector3d(msg.max_accel[0], msg.max_accel[1], msg.max_accel[2]);
  max_roll =  msg.max_roll;
  max_roll_rate = msg.max_roll_rate;
  max_psi_rate = msg.max_psi_rate;
}

uav_dynamics_constraints::DynamicsConstraints UAVDynamicsConstraints::DynamicsConstraintsToMsg() const{
  uav_dynamics_constraints::DynamicsConstraints msg;
  msg.max_velocity.push_back(max_velocity[0]);
  msg.max_velocity.push_back(max_velocity[1]);
  msg.max_velocity.push_back(max_velocity[2]);

  msg.max_accel.push_back(max_accel[0]);
  msg.max_accel.push_back(max_accel[1]);
  msg.max_accel.push_back(max_accel[2]);

  msg.max_roll = max_roll;
  msg.max_roll_rate = max_roll_rate;
  msg.max_psi_rate = max_psi_rate;
  return msg;
}

}



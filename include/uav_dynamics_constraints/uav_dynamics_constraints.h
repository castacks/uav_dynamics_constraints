/**
 * Copyright (c) 2015 Carnegie Mellon University, Sanjiban Choudhury <sanjiban@cmu.edu>
 *
 * For License information please see the LICENSE file in the root directory.
 *
 */

#ifndef UAV_DYNAMICS_CONSTRAINTS_INCLUDE_UAV_DYNAMICS_CONSTRAINTS_UAV_DYNAMICS_CONSTRAINTS_H_
#define UAV_DYNAMICS_CONSTRAINTS_INCLUDE_UAV_DYNAMICS_CONSTRAINTS_UAV_DYNAMICS_CONSTRAINTS_H_


#include <Eigen/Dense>
#include <ros/ros.h>
#include <rosparam_util/rosparam_util.h>
#include "uav_dynamics_constraints/DynamicsConstraints.h"

namespace ca {

/**
 * \brief Constraints for uav
 *
 */
class UAVDynamicsConstraints {
 public:
  /**
   * \brief default constructor
   */
  UAVDynamicsConstraints()
      : max_velocity(),
        max_accel(),
        max_roll(),
        max_roll_rate(),
        max_psi_rate() {
  }

  bool Initialize(ros::NodeHandle &n) {
    bool result = true;
    result = result
        && ca::rosparam_util::GetVectorParam<double, 3>(n, "max_velocity",
                                                        max_velocity);
    result = result
        && ca::rosparam_util::GetVectorParam<double, 3>(n, "max_accel",
                                                        max_accel);
    result = result && n.getParam("max_roll", max_roll);
    result = result && n.getParam("max_roll_rate", max_roll_rate);
    result = result && n.getParam("max_psi_rate", max_psi_rate);
    if (!result)
      ROS_ERROR_STREAM("Couldnt load helicopter constraints");
    return result;
  }

  double GetMinTurnRadius(double velocity) const {
    return velocity * velocity / (9.8 * tan(max_roll));
  }

  void DynamicsConstraintsFromMsg(const uav_dynamics_constraints::DynamicsConstraints &msg);

  uav_dynamics_constraints::DynamicsConstraints DynamicsConstraintsToMsg() const;

  Eigen::Vector3d max_velocity;
  Eigen::Vector3d max_accel;
  double max_roll;
  double max_roll_rate;
  double max_psi_rate;
};



}  // namespace ca



#endif /* UAV_DYNAMICS_CONSTRAINTS_INCLUDE_UAV_DYNAMICS_CONSTRAINTS_UAV_DYNAMICS_CONSTRAINTS_H_ */
